call plug#begin('~/.vim/plugged')
Plug 'sonph/onehalf', { 'rtp': 'vim' }
Plug 'wadackel/vim-dogrun'
Plug 'flrnd/candid.vim'
Plug 'preservim/nerdtree'
Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'jiangmiao/auto-pairs'
Plug 'https://github.com/bfrg/vim-cpp-modern.git'
Plug 'calviken/vim-gdscript3'
Plug 'ryanoasis/vim-devicons'
Plug 'bagrat/vim-buffet'
Plug 'mhinz/vim-startify'
Plug 'uiiaoo/java-syntax.vim'
call plug#end()

" Config
set completeopt=menuone,noinsert,noselect
set number
set nowrap
set tabstop=2
set shiftwidth=2
set expandtab
set encoding=UTF-8
set t_Co=256
set t_ut=
if exists('+termguicolors')
  let &t_8f = "\<Esc>[38;2;%lu;%lu;%lum"
  let &t_8b = "\<Esc>[48;2;%lu;%lu;%lum"
  set termguicolors
endif
colorscheme onehalfdark
hi! EndOfBuffer ctermbg=bg ctermfg=bg guibg=bg guifg=bg
set hlsearch!
nnoremap <F3> :set hlsearch!<CR>
inoremap <C-s> <Esc>:w<CR>a
inoremap <C-Up> <Esc>ddko
nnoremap <C-s> :w<CR>
nnoremap <C-e> :bd<CR>
nnoremap <C-q> :wq<CR>

" NERDTree
nnoremap <tab> :NERDTreeToggle<CR>

" Buffet
function! g:BuffetSetCustomColors()
  hi! BuffetCurrentBuffer ctermbg=255 ctermfg=255 guibg=255 guifg=255
  hi! BuffetActiveBuffer ctermbg=255 ctermfg=255 guibg=255 guifg=255
  hi! BuffetBuffer ctermbg=NONE guibg=#1F2228 guifg=#909090
  hi! BuffetModCurrentBuffer ctermbg=255 ctermfg=255 guibg=255 guifg=255
  hi! BuffetModeActiveBuffer ctermbg=255 ctermfg=255 guibg=255 guifg=255
  hi! BuffetModBuffer ctermbg=255 ctermfg=255 guibg=255 guifg=255
  hi! BuffetTrunc ctermbg=255 ctermfg=255 guibg=255 guifg=255
  hi! BuffetTab ctermbg=255 ctermfg=255 guibg=255 guifg=bg
endfunction
let g:buffet_separator = ''
let g:buffet_show_index = 0
let g:buffet_max_plug = 10
let g:buffet_modified_icon = ' '
noremap <S-Right> :bn<CR>
noremap <S-Left> :bp<CR>

" Ejecutar
nnoremap <F5> :call ExecuteFile()<CR>

function! ExecuteFile()
  let filename = expand("%:t:r")
  let fileexte = expand("%:t:e")
  let filepath = expand("%:p:h")

  if fileexte == "py"
    exec ":term "."python ".filepath."/".filename.".".fileexte
  elseif fileexte == "java"
    let proyecto = 0
    for part in split(filepath, "/")
      if part == "src"
        let proyecto = 1
        break
      endif
    endfor
    if proyecto
      let ruta = "/"
      let paquete = ""
      let encontrado = 0
      for part in split(filepath, "/")
        if part == "src"
          let encontrado = 1
          continue
        endif
        if encontrado
          let paquete = paquete.part."/"
        else
          let ruta = ruta.part."/"
        endif
      endfor
      exec ":term javac -d ".ruta."out/production/ed_2021_1/ --source-path ".ruta."src"." ".ruta."src/".paquete.filename.".".fileexte." && java -cp ".ruta."out/production/ed_2021_1/ ".paquete.filename
    else
      exec ":term "."javac ".filepath."/".filename.".".fileexte." && java -cp ".filepath."/ ".filename
    endif
  elseif fileexte == "cs"
    exec ":term "."dotnet run --project ".filepath
  elseif fileexte == "cp"
    exec ":term g++ ".filepath."/".filename.".".fileexte." -o ".filepath."/".filename.".out"." && ".filepath."/".filename.".out"
  elseif fileexte == "cpp"
    exec ":term g++ ".filepath."/".filename.".".fileexte." -o ".filepath."/".filename.".out"." && ".filepath."/".filename.".out"
  elseif fileexte == "cc"
    exec ":term g++ ".filepath."/".filename.".".fileexte." -o ".filepath."/".filename.".out"." && ".filepath."/".filename.".out"
  endif
endfunction

" Startify
let g:startify_lists = [
          \ { 'type': 'files',     'header': ['   Files']            },
          \ { 'type': 'dir',       'header': ['   Current Directory '. getcwd()] },
          \ { 'type': 'sessions',  'header': ['   Sessions']       },
          \ { 'type': 'bookmarks', 'header': ['   Bookmarks']      },
          \ ]

let g:startify_bookmarks = [
            \ { 'c': '~/.config/i3/config' },
            \ { 'i': '~/.config/nvim/init.vim' },
            \ ]

" Coc
" TextEdit might fail if hidden is not set.
set hidden

" Some servers have issues with backup files, see #649.
set nobackup
set nowritebackup

" Give more space for displaying messages.
set cmdheight=2

" Having longer updatetime (default is 4000 ms = 4 s) leads to noticeable
" delays and poor user experience.
set updatetime=300

" Don't pass messages to |ins-completion-menu|.
set shortmess+=c

" Always show the signcolumn, otherwise it would shift the text each time
" diagnostics appear/become resolved.
if has("patch-8.1.1564")
  " Recently vim can merge signcolumn and number column into one
  set signcolumn=number
else
  set signcolumn=yes
endif

" Use tab for trigger completion with characters ahead and navigate.
" NOTE: Use command ':verbose imap <tab>' to make sure tab is not mapped by
" other plugin before putting this into your config.
inoremap <silent><expr> <TAB>
      \ pumvisible() ? "\<C-n>" :
      \ <SID>check_back_space() ? "\<TAB>" :
      \ coc#refresh()
inoremap <expr><S-TAB> pumvisible() ? "\<C-p>" : "\<C-h>"

function! s:check_back_space() abort
  let col = col('.') - 1
  return !col || getline('.')[col - 1]  =~# '\s'
endfunction

" Use <c-space> to trigger completion.
if has('nvim')
  inoremap <silent><expr> <c-space> coc#refresh()
else
  inoremap <silent><expr> <c-@> coc#refresh()
endif

" Make <CR> auto-select the first completion item and notify coc.nvim to
" format on enter, <cr> could be remapped by other vim plugin
inoremap <silent><expr> <cr> pumvisible() ? coc#_select_confirm()
                              \: "\<C-g>u\<CR>\<c-r>=coc#on_enter()\<CR>"
inoremap <expr> <CR> complete_info().selected != -1 ?
            \ &filetype == "gdscript3" ? (coc#expandable() ?  "\<C-y>" : "\<C-y><Esc>a") : "\<C-y>"
            \ : "\<C-g>u\<CR>"

" Use `[g` and `]g` to navigate diagnostics
" Use `:CocDiagnostics` to get all diagnostics of current buffer in location list.
nmap <silent> [g <Plug>(coc-diagnostic-prev)
nmap <silent> ]g <Plug>(coc-diagnostic-next)

" GoTo code navigation.
nmap <silent> gd <Plug>(coc-definition)
nmap <silent> gy <Plug>(coc-type-definition)
nmap <silent> gi <Plug>(coc-implementation)
nmap <silent> gr <Plug>(coc-references)

" Use K to show documentation in preview window.
nnoremap <silent> K :call <SID>show_documentation()<CR>

function! s:show_documentation()
  if (index(['vim','help'], &filetype) >= 0)
    execute 'h '.expand('<cword>')
  elseif (coc#rpc#ready())
    call CocActionAsync('doHover')
  else
    execute '!' . &keywordprg . " " . expand('<cword>')
  endif
endfunction

" Highlight the symbol and its references when holding the cursor.
autocmd CursorHold * silent call CocActionAsync('highlight')

" Symbol renaming.
nmap <leader>rn <Plug>(coc-rename)

" Formatting selected code.
xmap <leader>f  <Plug>(coc-format-selected)
nmap <leader>f  <Plug>(coc-format-selected)

augroup mygroup
  autocmd!
  " Setup formatexpr specified filetype(s).
  autocmd FileType typescript,json setl formatexpr=CocAction('formatSelected')
  " Update signature help on jump placeholder.
  autocmd User CocJumpPlaceholder call CocActionAsync('showSignatureHelp')
augroup end

" Applying codeAction to the selected region.
" Example: `<leader>aap` for current paragraph
xmap <leader>a  <Plug>(coc-codeaction-selected)
nmap <leader>a  <Plug>(coc-codeaction-selected)

" Remap keys for applying codeAction to the current buffer.
nmap <leader>ac  <Plug>(coc-codeaction)
" Apply AutoFix to problem on the current line.
nmap <leader>qf  <Plug>(coc-fix-current)

" Map function and class text objects
" NOTE: Requires 'textDocument.documentSymbol' support from the language server.
xmap if <Plug>(coc-funcobj-i)
omap if <Plug>(coc-funcobj-i)
xmap af <Plug>(coc-funcobj-a)
omap af <Plug>(coc-funcobj-a)
xmap ic <Plug>(coc-classobj-i)
omap ic <Plug>(coc-classobj-i)
xmap ac <Plug>(coc-classobj-a)
omap ac <Plug>(coc-classobj-a)

" Add `:Format` command to format current buffer.
command! -nargs=0 Format :call CocAction('format')

" Add `:Fold` command to fold current buffer.
command! -nargs=? Fold :call     CocAction('fold', <f-args>)

" Add `:OR` command for organize imports of the current buffer.
command! -nargs=0 OR   :call     CocAction('runCommand', 'editor.action.organizeImport')

" Add (Neo)Vim's native statusline support.
" NOTE: Please see `:h coc-status` for integrations with external plugins that
" provide custom statusline: lightline.vim, vim-airline.
set statusline^=%{coc#status()}%{get(b:,'coc_current_function','')}

" Mappings for CoCList
" Show all diagnostics.
nnoremap <silent><nowait> <space>a  :<C-u>CocList diagnostics<cr>
" Manage extensions.
nnoremap <silent><nowait> <space>e  :<C-u>CocList extensions<cr>
" Show commands.
nnoremap <silent><nowait> <space>c  :<C-u>CocList commands<cr>
" Find symbol of current document.
nnoremap <silent><nowait> <space>o  :<C-u>CocList outline<cr>
" Search workspace symbols.
nnoremap <silent><nowait> <space>s  :<C-u>CocList -I symbols<cr>
" Do default action for next item.
nnoremap <silent><nowait> <space>j  :<C-u>CocNext<CR>
" Do default action for previous item.
nnoremap <silent><nowait> <space>k  :<C-u>CocPrev<CR>
" Resume latest coc list.
nnoremap <silent><nowait> <space>p  :<C-u>CocListResume<CR>
